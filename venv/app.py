from flask import Flask,render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'

db = SQLAlchemy(app)

class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    intro = db.Column(db.String(300), nullable=False)
    text = db.Column(db.Text, nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    #чтобы получать строки из бд
    def __repr__(self):
        return '<Article %r>' %self.id

@app.route('/')
@app.route('/home')
def index():
    return render_template("index.html")

@app.route('/about')
def about():
    return render_template("about.html")

@app.route('/posts')
def posts():
    arts = Article.query.order_by(Article.date.desc()).all() #first для первой, all - для всех
    return render_template("posts.html", arts=arts)

@app.route('/posts/<int:id>')
def posts_exp(id):
    art = Article.query.get(id)
    return render_template("posts_exp.html", art=art)

@app.route('/posts/<int:id>/delete')
def posts_del(id):
    art = Article.query.get_or_404(id)

    try:
        db.session.delete(art)
        db.session.commit()
        return redirect("/posts")
    except:
        return "Err with deleting article!"


@app.route('/posts/<int:id>/update', methods=['POST','GET'])
def post_upd(id):
    art = Article.query.get(id)
    if request.method == "POST":
        art.title = request.form['title']
        art.intro = request.form['intro']
        art.text = request.form['text']

        try:
            db.session.commit()
            return redirect('/posts')
        except:
            return "Err with edit"
    else:
        return render_template("post_update.html", article=art)


@app.route('/create-article', methods=['POST','GET'])
def create_article():
    if request.method == "POST":
        title = request.form['title']
        intro = request.form['intro']
        text = request.form['text']

        art = Article(title=title, intro=intro, text=text)

        try:
            db.session.add(art)
            db.session.commit()
            return redirect('/posts')
        except:
            return "Ошибка при добавлении"
    else:
        return render_template("create_article.html")


if __name__ == "__main__":
    app.run(debug=True)